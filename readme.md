## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)




### Shopee Requirement
### ==================


Dear Candidates

Congratulations! 
Regarding your submission on our website, we believe you can have the technical test as the first assessment for Web Developer position at Shopee Indonesia.

Attached below the files you have to work on, in the next 1x24 hours. Please zip your source code and send it to us. For all pages, please prioritize on how it would look on mobile – most of our users are smartphone users.
 You will also need to create your own database structure. Include the script that you would use to create the necessary database and table / collection, for the purposes of this application. 

Index Page
Index page design is attached as an Adobe Illustrator file – please implement this design as  index page as how you would see fit. The design is mobile-focused – a fast loading time is preferred. At the same time, please also consider how PC users would see this website in a user-friendly manner. 

There are two links on the middle of the page. “Ketentuan dan Kebijakan Shopee” should link to here, while the bottom “Cek Kriteria Lengkap di Sini” should link to here.

There is one CTA (button “Gabung Sekarang”) at the bottom of the index page, which is clickable and will redirect user to Login Page.

**Login Page**  
Sellers need to login to Shopee first before they can register to our website. The requirements for this login page are:  
1.	It has two fields (username & password) & one login button.  
2.	Username field length must be between 6-15 character, and only alphabet and numeric are allowed.  
3.	Password field length must be between 8-16 character.  
4.	Validate if user exist in the database.  
5.	Validation must be done on client-side and server-side, and authentication must be done on server side.   
6.	If validation and/or authentication failed, display an error message. Include a link so user can register on the website.  
7.	Once data is valid and user is authenticated, continue to the next page (Preferred Seller Signup). Store username data for signup at the next page.  
  

**Preferred Seller Signup Page**  
Sellers must login first before they can access this page – usually they are redirected from Login page. The requirements for this signup page are:  
1.	It has three inputs: KTP number, photo of user, and photo of KTP  
2.	KTP number is 16 digit long – only numeric characters allowed.  
3.	Both photos’ size must be at most 100 kB, and user can select & upload the photo from their device (either PC / mobile). Accepted format: JPG and PNG.  
4.	Validation must be done on server-side and client-side.  
5.	Once done, insert the data (username, KTP number, and filenames of both photos) to a new table/collection (different from the users table), and redirect user to Signup Complete Page.  
6.	Again, you don’t need to upload the photo or store it – just need to store the filename of both photos.  
  

**Signup Complete Page**  
In this page, display a thank you message, and redirect user to http://shopee.co.id after 5 seconds. If user have already signed up for Preferred Seller before and tried to login at Login Page again, skip all pages and automatically redirect user to this page instead.  
  

**Shopee User Registration Page**  
If users does not have a Shopee username, they can register here. In this page, you have to follow the instructions below:   
1.	There are 4 fields: username, email, password, and password confirmation.  
2.	Username field length must between 6-15 character, unique and only alphabet and numeric are allowed.  
3.	Email field must follow a valid e-mail format (e.g. test123@gmail.com).  
4.	Password field length must between 8-16 character.  
5.	Password and password confirmation must have same value.  
6.	Validation must be done on server-side and client-side.  
7.	Once data is valid, insert the data to backend database (users table / collection), and redirect user to Preferred Seller signup page.  
  


### Checklist & Grading Guideline

You will be graded on the following aspects. Do note that this checklist can also be used for your own reference, to assess how complete your website is.  
Feel free to prioritize based on your strongest skills and your interests.  
  

**FE**  
(1) Does your website UI follow the overall design, from Index page to Shopee Registration Page?  
(2) Does each button (next / confirm / submit / file upload) behave as intended?  
(3) Is the whole website responsive? Can it be viewed in PC as well as in mobile?  
(4) Does your website handle form submission to the BE correctly?  
(5) Are all validations (on the client-side) completed?  
(6) Did you show any error messages to user, either due to failed client-side or server-side validation?  
(7) Have you managed to slice the given AI design when implementing the index page?  
(8) Would the loading time of the index page be fast, especially for mobile/3G internet connection?  
  

**BE**  
(1) Is there a script to set up the databases / tables / collections?  
(2) Does your BE receive form submission result from FE correctly?  
(3) Are all validations (on the server-side) completed?  
(4) Are all submitted data being processed and stored to database correctly?  
(5) Did you sanitise all inputs that were submitted by the end user?  
(6) Did you implement any error handling that can be passed to FE?  
(7) Did you secure any and all login information passed from user all the way to database (and v.v.)?  
(8) Is your website easily extendable to cater for additional requirements?  
  

**Docu-mentation**  
(1) Have you documented your tech stacks (programming language / framework / database choices)?  
(2) Are there instructions to set up everything properly?  
(3) Is your code well documented / self-explaining?  
(4) Will we be able to understand your code without deploying it?  
  


